# websocket-unity

The project provides a library that enables websockets in unity for all platforms (including webgl).

### Used libraries

We use the following libraries:

[websocket-sharp](https://github.com/sta/websocket-sharp): provides basic websocket capabilities (it is included as .dll file which we created based on its current sources 2018/30/11)

[unity-websocket-webgl](https://github.com/jirihybek/unity-websocket-webgl): extends [websocket-sharp](https://github.com/sta/websocket-sharp) to compile to webgl

### Our extensions

In addition to [unity-websocket-webgl](https://github.com/jirihybek/unity-websocket-webgl) this project provides the following features:

1. Configuration of wss connections
1. Support to send and receive string messages
2. Required projects to use wss for native and web platforms are bundled together


## Install plugin

Copy contents:

* libs/websocket-sharp.dll
* WebSocket.cs
* WebSocket.jslib

into your Plugins folder.

## Usage

```csharp

// create a secure websocket instance
HybridWebSocket.WebSocket ws = WebSocketBuilder.Builder().Url("wss://<url>")
            .EnableSsl(System.Security.Authentication.SslProtocols.Tls12)
            .Build();

// handle incoming websocket message
ws.OnMessage += (byte[] msg) =>
{
    var asStr = Encoding.UTF8.GetString(msg);
    Debug.Log("WS received message: " + asStr);
};

// connect to the websocket
ws.Connect();

```

## Maintainer

* [Phibart](https://gitlab.com/phibart)
* [Josch Rossa](https://gitlab.com/josros)